<?php


namespace App\Services;
use App\SchoolBoard;
use SimpleXMLElement;


class GradeCalculation
{
    /**
     * @var int
     */
    private $total;

    /**
     * @var int
     */
    private $count;

    /**
     * @var \App\Student::class
     */
    private $student;

    /**
     * @var array
     */
    private $grades = [];

    /**
     * @var array
     */
    private $resultList = ['id' => '', 'name' => '', 'grades' => [], 'average' => '', 'pass' => ''];

    /**
     * CSM considers pass if the average is bigger or equal to 7 and fail otherwise.
     * Returns JSON format
     *
     * @return json
     */
    private function csmGradeCalculator() {
        $this->popGradeRefs();
        $this->popResponse();
        $this->resultList['pass'] = $this->resultList['average'] >= 7 ? 'Yes' : 'No';

        return response()->json($this->resultList);
    }

    /**
     * CSMB discards the lowest grade, if you have more than 2 grades,
     * and considers pass if his biggest grade is bigger than 8. Returns XML format
     *
     * @return xml
     */
    private function csmbGradeCalculator() {
        $this->popGradeRefs();
        $this->popResponse();

        if ($this->count > 2) {
            array_shift($this->grades);
            $this->count--;
        }

        $this->total = array_sum($this->grades);
        $this->resultList['pass'] = max($this->grades) > 8 ? 'Yes' : 'No';
        $this->resultList['grades'] = implode(', ', $this->grades);

        $this->generateXML();

        return response(file_get_contents(public_path('student.xml')), 200, [
            'Content-Type' => 'application/xml'
        ]);
    }

    /**
     * Mutates the array in App\Student::$grades so we can sort it
     * alpha numerically and reference with class instance
     */
    private function popGradeRefs() {
        $this->grades = $this->student->grades;
        sort($this->grades, SORT_NUMERIC);
        $this->total = array_sum($this->grades);
    }

    /**
     * Populates the variable @resultsList
     */
    private function popResponse() {
        $this->resultList['name'] = $this->student->name;
        $this->resultList['id'] = $this->student->id;
        $this->resultList['grades'] = $this->student->grades;
        $this->resultList['average'] = $this->total/$this->count;
    }

    /**
     * Generates the XML file and stores it into the public folder.
     */
    private function generateXML() {
        $xmlData = new SimpleXMLElement('<?xml version="1.0"?><data></data>');
        $this->convertArrayToXml($this->resultList,$xmlData);
        $xmlData->asXML(public_path('student.xml'));
    }

    /**
     * Find the reference requested
     *
     * @return App\Student::class
     */
    private function getSchoolBoardType() {
        return SchoolBoard::find($this->student->school_board_id);
    }

    /**
     * @return json/xml
     */
    public function getGradeAverage($student) {
        $this->student = $student;
        $this->count = count($student->grades);
        $board = $this->getSchoolBoardType();

        return $board->name == 'csm' ? $this->csmGradeCalculator() : $this->csmbGradeCalculator();
    }

    /**
     * Converts a PHP array to a XML
     *
     * @param $array
     * @param $xml_user_info
     */
    private function convertArrayToXml($array, &$xml_user_info) {
        foreach($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml_user_info->addChild("$key");
                    $this->convertArrayToXml($value, $subnode);
                } else{
                    $subnode = $xml_user_info->addChild("item$key");
                    $this->convertArrayToXml($value, $subnode);
                }
            } else {
                $xml_user_info->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }
}
