<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use App\Services\GradeCalculation;
use Illuminate\Support\Facades\Validator;


class StudentController extends Controller
{

    /*
     * @var App\Services\GradeCalculation::class
     */
    private $gradeCalculator;


    /**
     * StudentController constructor.
     * @param GradeCalculation $gradeCalculation
     */
    public function __construct(GradeCalculation $gradeCalculation)
    {
        $this->gradeCalculator = $gradeCalculation;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Calculates the average grade based on the school board system.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return $this->gradeCalculator->getGradeAverage($student);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('student.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $values = $this->validateRequest();
        $grades = [
            'grades' => array_filter(array_values($values))
        ];
        $student->update($grades);
        $path = "student/$student->id/edit";

        return redirect($path)->with('success', 'Student grades updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }

    /**
     * Validate the request attributes.
     *
     * @return array
     */
    protected function validateRequest()
    {
        return request()->validate([
            'grade_0' => 'nullable|integer|between:6,10',
            'grade_1' => 'nullable|integer|between:6,10',
            'grade_2' => 'nullable|integer|between:6,10',
            'grade_3' => 'nullable|integer|between:6,10',
        ]);
    }
}
