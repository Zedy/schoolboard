<?php

namespace App\Http\Controllers;

use App\SchoolBoard;
use Illuminate\Http\Request;

class SchoolBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schoolBoards = \App\SchoolBoard::all();
        return view('board.index', compact('schoolBoards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param SchoolBoard $board
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolBoard $board)
    {
        return view('board.show', compact('board'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SchoolBoard  $schoolBoard
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolBoard $board)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SchoolBoard  $board
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolBoard $board)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SchoolBoard  $board
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolBoard $board)
    {
        //
    }
}
