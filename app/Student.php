<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Student extends Model
{
    protected $table = 'students';

    protected $casts = [
        'grades' => 'array'
    ];

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schoolboard() {
        return $this->belongsTo(SchoolBoard::class);
    }

    /**
     * Exposes the path to each model, so we don't have to
     * write each manually in the view
     *
     * @return string
     */
    public function path() {
        return "/student/$this->id";
    }
}
