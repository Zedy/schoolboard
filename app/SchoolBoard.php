<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolBoard extends Model
{
    protected $table = 'school_boards';

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function path()
    {
        return "/boards/{$this->id}";
    }
}
