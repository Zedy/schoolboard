@extends('layouts.app')

@section('title', 'Edit student grades')

@section('content')
    @include('flash-message')
    <h1 class="mt-10">{{ $student->name }}</h1>
    <form method="POST" action="{{ $student->path() }}">
        @method('PATCH')
        @csrf

        <h4>Grades list:</h4>
        <div class="form-group">
            <input class="form-control" name="grade_0" type="text" value="{{ array_key_exists(0, $student->grades) ? old('grade_0') ? old('grade_0') : $student->grades[0] : '' }}"/>
            @error('grade_0')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <input class="form-control" name="grade_1" type="text" value="{{ array_key_exists(1, $student->grades) ? old('grade_1') ? old('grade_1') : $student->grades[1] : '' }}"/>
            @error('grade_1')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <input class="form-control" name="grade_2" type="text" value="{{ array_key_exists(2, $student->grades) ? old('grade_2') ? old('grade_2') : $student->grades[2] : '' }}"/>
            @error('grade_2')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <input class="form-control" name="grade_3" type="text" value="{{ array_key_exists(3, $student->grades) ? old('grade_3') ? old('grade_3') : $student->grades[3] : '' }}"/>
            @error('grade_3')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <p><strong>Notice:</strong> a student may have up to four (4) grades at any time.</p>
        <button class="btn btn-primary" type="submit">Submit</button>
        <a class="btn btn-light" href="/boards/{{ $student->school_board_id}}">Go back</a>
    </form>
@endsection
