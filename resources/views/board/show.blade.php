@extends('layouts.app')

@section('title', 'Student overview')

@section('content')
    <h1>Board: {{ strtoupper($board->name) }}</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Grades</th>
            <th scope="col">CTA</th>
        </tr>
        </thead>
        <tbody>
        @foreach($board->students as $student)
            <tr>
                <th scope="row">{{ $loop->index +1 }}</th>
                <td>{{ $student->id }}</td>
                <td>{{ $student->name }}</td>
                <td>
                    {{ implode(', ', $student->grades)}}
                </td>
                <td>
                    <a class="btn btn-light" href="{{ $student->path() }}/edit">{{ count($student->grades) ? 'Edit' : 'Input' }} grades</a>
                    <a class="btn btn-light {{ count($student->grades) ? '' : 'disabled' }}"
                       href="{{ count($student->grades) ? $student->path() : 'javascript:;' }}">Calculate average</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
