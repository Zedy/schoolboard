@extends('layouts.app')

@section('title', 'Board overview')

@section('content')
    <div class="schoolboard-overview">
        @foreach($schoolBoards as $board)
            <div>
                <a href="{{ $board->path() }}">{{ $board->name }}</a>
            </div>
        @endforeach
    </div>
@endsection
