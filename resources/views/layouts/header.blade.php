<header class="masthead mb-auto">
    <div class="inner">
        <h3 class="masthead-brand"><a href="/">{{ config('app.name') }}</a></h3>
        <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link {{ (request()->is('boards')) ? 'active' : '' }}" href="/boards">Boards overview</a>
            @if (Route::has('login'))
                    @auth
                        <a class="nav-link {{ (request()->is('logout')) ? 'active' : '' }}" href="{{ url('/logout') }}">Logout</a>
                    @else
                        <a class="nav-link {{ (request()->is('login')) ? 'active' : '' }}" href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a class="nav-link {{ (request()->is('register')) ? 'active' : '' }}" href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
            @endif
        </nav>
    </div>
</header>
