<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique(true)->safeEmail,
        'grades' => $faker->randomElements([6, 7, 8, 9, 10], $count = rand(1,4), true),
        'school_board_id' => $faker->unique()->randomElement([1, 2]) // we only have two (2) boards so we need to seed ids with those values not random ones from a new factory
    ];
});
