<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SchoolBoard;
use Faker\Generator as Faker;

$factory->define(SchoolBoard::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(['csm', 'csmb']),
    ];
});
