<?php

use Illuminate\Database\Seeder;
use App\SchoolBoard;

class SchoolBoardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(['csm', 'csmb'])->each(function ($schoolboard) {
            factory(App\SchoolBoard::class)->create([
                'name' => $schoolboard,
            ]);
        });
    }
}
