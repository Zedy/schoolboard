<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::resource('boards', 'SchoolBoardController');
    Route::resource('student', 'StudentController');
});

Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('login');
});

Route::get('/', function(){
    return Redirect::to('boards');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
