# Project: SchoolBoard

##Project installation
###1) Database
Either use the link provided bellow to download the database used during development or create a new database [collation `utf8mb4_general_ci`] and use the `factory` (Step 2) to seed the database with fresh data.

Database download [LINK](https://drive.google.com/file/d/1vkBpKA9cfV-_6UKke_Wvoe5oAlo0__hY/view?usp=sharing)
#####1a) Database import
- Create a new empty database
- Run `mysql -u {username} -p {password} database_name < schoolboard.sql`
#####1b) Manual database creation
- Create a new empty database 
- Set the necessary settings within the `.env` file
- Run `composer install` to install the Laravel dependacies
- Run the migrations `php artisan migrate`
- Run `php artisan tinker` followed by commands:

    factory(App\SchoolBoard::class, 2)->create(); // Do not modify integer!!!
    factory(App\Student::class, 30)->create(); // Integer determines the number of students (range 1-1000) exceeding 1000 can cause a timeout
     
This will create the necessay data for the project to function as excepted.

###2) Project Setup
- Create a new `.env` file from the existing `.env.example` (Enter a meaningful string for `APP_NAME`)
- Run `composer install` to install the Laravel dependacies (if not done so in previous step)
- Run `npm install` to install node_module dependacies
- Run `npm run dev` to compile frontend resources
- Run `php artisan key:generate` to generate the app:key
- Run `php artisan serve`
- Once the project is up&running create a new user and start using the application

###3) Using the app
App has a fully functional UI for ease of use and should be relatively easy to use (I HOPE :) )


